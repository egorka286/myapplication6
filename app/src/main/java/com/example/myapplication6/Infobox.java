package com.example.myapplication6;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class Infobox implements BankObject {

    @SerializedName("info_id")
    @Expose
    private Integer infoId;
    @SerializedName("area")
    @Expose
    private String area;
    @SerializedName("city_type")
    @Expose
    private String cityType;
    @SerializedName("city")
    @Expose
    private String city;
    @SerializedName("address_type")
    @Expose
    private String addressType;
    @SerializedName("address")
    @Expose
    private String address;
    @SerializedName("house")
    @Expose
    private String house;
    @SerializedName("install_place")
    @Expose
    private String installPlace;
    @SerializedName("location_name_desc")
    @Expose
    private String locationNameDesc;
    @SerializedName("work_time")
    @Expose
    private String workTime;
    @SerializedName("time_long")
    @Expose
    private String timeLong;
    @SerializedName("gps_x")
    @Expose
    private String gpsX;
    @SerializedName("gps_y")
    @Expose
    private String gpsY;
    @SerializedName("currency")
    @Expose
    private String currency;
    @SerializedName("inf_type")
    @Expose
    private String infType;
    @SerializedName("cash_in_exist")
    @Expose
    private String cashInExist;
    @SerializedName("cash_in")
    @Expose
    private String cashIn;
    @SerializedName("type_cash_in")
    @Expose
    private String typeCashIn;
    @SerializedName("inf_printer")
    @Expose
    private String infPrinter;
    @SerializedName("region_platej")
    @Expose
    private String regionPlatej;
    @SerializedName("popolnenie_platej")
    @Expose
    private String popolneniePlatej;
    @SerializedName("inf_status")
    @Expose
    private String infStatus;

    @Override
    public String getAddress() {
        return addressType + address + " " + house;
    }

    @Override
    public gpsPoint getGpsPoint() {
        return new gpsPoint(Double.parseDouble(gpsX), Double.parseDouble(gpsY));
    }

    @Override
    public int getTypeId() {
        return R.string.infobox;
    }
}