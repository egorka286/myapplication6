package com.example.myapplication6;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class Filial implements BankObject {

    @SerializedName("filial_id")
    @Expose
    private String filialId;
    @SerializedName("sap_id")
    @Expose
    private String sapId;
    @SerializedName("filial_name")
    @Expose
    private String filialName;
    @SerializedName("name_type")
    @Expose
    private String nameType;
    @SerializedName("name")
    @Expose
    private String name;
    @SerializedName("street_type")
    @Expose
    private String streetType;
    @SerializedName("street")
    @Expose
    private String street;
    @SerializedName("home_number")
    @Expose
    private String homeNumber;
    @SerializedName("name_type_prev")
    @Expose
    private Object nameTypePrev;
    @SerializedName("name_prev")
    @Expose
    private Object namePrev;
    @SerializedName("street_type_prev")
    @Expose
    private String streetTypePrev;
    @SerializedName("street_prev")
    @Expose
    private String streetPrev;
    @SerializedName("home_number_prev")
    @Expose
    private String homeNumberPrev;
    @SerializedName("info_text")
    @Expose
    private String infoText;
    @SerializedName("info_worktime")
    @Expose
    private String infoWorktime;
    @SerializedName("info_bank_bik")
    @Expose
    private Object infoBankBik;
    @SerializedName("info_bank_unp")
    @Expose
    private Object infoBankUnp;
    @SerializedName("GPS_X")
    @Expose
    private String gpsX;
    @SerializedName("GPS_Y")
    @Expose
    private String gpsY;
    @SerializedName("bel_number_schet")
    @Expose
    private String belNumberSchet;
    @SerializedName("foreign_number_schet")
    @Expose
    private String foreignNumberSchet;
    @SerializedName("phone_info")
    @Expose
    private String phoneInfo;
    @SerializedName("info_weekend1_day")
    @Expose
    private String infoWeekend1Day;
    @SerializedName("info_weekend2_day")
    @Expose
    private String infoWeekend2Day;
    @SerializedName("info_weekend3_day")
    @Expose
    private String infoWeekend3Day;
    @SerializedName("info_weekend4_day")
    @Expose
    private String infoWeekend4Day;
    @SerializedName("info_weekend5_day")
    @Expose
    private String infoWeekend5Day;
    @SerializedName("info_weekend6_day")
    @Expose
    private String infoWeekend6Day;
    @SerializedName("info_weekend7_day")
    @Expose
    private String infoWeekend7Day;
    @SerializedName("info_weekend1_time")
    @Expose
    private String infoWeekend1Time;
    @SerializedName("info_weekend2_time")
    @Expose
    private String infoWeekend2Time;
    @SerializedName("info_weekend3_time")
    @Expose
    private String infoWeekend3Time;
    @SerializedName("info_weekend4_time")
    @Expose
    private String infoWeekend4Time;
    @SerializedName("info_weekend5_time")
    @Expose
    private String infoWeekend5Time;
    @SerializedName("info_weekend6_time")
    @Expose
    private String infoWeekend6Time;
    @SerializedName("info_weekend7_time")
    @Expose
    private String infoWeekend7Time;
    @SerializedName("dop_num")
    @Expose
    private String dopNum;
    @SerializedName("strahovanie_med_rashodov")
    @Expose
    private String strahovanieMedRashodov;
    @SerializedName("usl_broker")
    @Expose
    private String uslBroker;
    @SerializedName("usl_buy_slitki")
    @Expose
    private String uslBuySlitki;
    @SerializedName("usl_card_internet")
    @Expose
    private String uslCardInternet;
    @SerializedName("usl_cennie_bumagi")
    @Expose
    private String uslCennieBumagi;
    @SerializedName("usl_check_dover_vnebanka")
    @Expose
    private String uslCheckDoverVnebanka;
    @SerializedName("usl_cheki_gilie")
    @Expose
    private String uslChekiGilie;
    @SerializedName("usl_cheki_imuschestvo")
    @Expose
    private String uslChekiImuschestvo;
    @SerializedName("usl_club_barhat")
    @Expose
    private String uslClubBarhat;
    @SerializedName("usl_club_kartblansh")
    @Expose
    private String uslClubKartblansh;
    @SerializedName("usl_club_ledi")
    @Expose
    private String uslClubLedi;
    @SerializedName("usl_club_nastart")
    @Expose
    private String uslClubNastart;
    @SerializedName("usl_club_persona")
    @Expose
    private String uslClubPersona;
    @SerializedName("usl_club_schodry")
    @Expose
    private String uslClubSchodry;
    @SerializedName("usl_coins_exchange")
    @Expose
    private String uslCoinsExchange;
    @SerializedName("usl_depositariy")
    @Expose
    private String uslDepositariy;
    @SerializedName("usl_dep_doverennosti")
    @Expose
    private String uslDepDoverennosti;
    @SerializedName("usl_dep_scheta")
    @Expose
    private String uslDepScheta;
    @SerializedName("usl_dep_viplati")
    @Expose
    private String uslDepViplati;
    @SerializedName("usl_docObligac_belarusbank")
    @Expose
    private String uslDocObligacBelarusbank;
    @SerializedName("usl_dover_upr")
    @Expose
    private String uslDoverUpr;
    @SerializedName("usl_dover_upr_gos")
    @Expose
    private String uslDoverUprGos;
    @SerializedName("usl_drag_metal")
    @Expose
    private String uslDragMetal;
    @SerializedName("usl_ibank")
    @Expose
    private String uslIbank;
    @SerializedName("usl_inkasso_priem")
    @Expose
    private String uslInkassoPriem;
    @SerializedName("usl_inkasso_priem_deneg_bel")
    @Expose
    private String uslInkassoPriemDenegBel;
    @SerializedName("usl_inkasso_vyplata")
    @Expose
    private String uslInkassoVyplata;
    @SerializedName("usl_int_cards")
    @Expose
    private String uslIntCards;
    @SerializedName("usl_izbiz_scheta_operacii")
    @Expose
    private String uslIzbizSchetaOperacii;
    @SerializedName("usl_izbiz_scheta_otkr")
    @Expose
    private String uslIzbizSchetaOtkr;
    @SerializedName("usl_kamni_brill")
    @Expose
    private String uslKamniBrill;
    @SerializedName("usl_kompleksny_product")
    @Expose
    private String uslKompleksnyProduct;
    @SerializedName("usl_konversiya_foreign_val")
    @Expose
    private String uslKonversiyaForeignVal;
    @SerializedName("usl_loterei")
    @Expose
    private String uslLoterei;
    @SerializedName("usl_mo_rb")
    @Expose
    private String uslMoRb;
    @SerializedName("usl_operations_bezdokumentar_obligacii")
    @Expose
    private String uslOperationsBezdokumentarObligacii;
    @SerializedName("usl_operations_po_schetam_belpochta")
    @Expose
    private String uslOperationsPoSchetamBelpochta;
    @SerializedName("usl_operations_sber_sertif")
    @Expose
    private String uslOperationsSberSertif;
    @SerializedName("usl_perechislenie_po_rekvizitam_kartochki")
    @Expose
    private String uslPerechisleniePoRekvizitamKartochki;
    @SerializedName("usl_plategi")
    @Expose
    private String uslPlategi;
    @SerializedName("usl_podlinnost_banknot")
    @Expose
    private String uslPodlinnostBanknot;
    @SerializedName("usl_pogashenie_documentar_obligacii")
    @Expose
    private String uslPogashenieDocumentarObligacii;
    @SerializedName("usl_popolnenieSchetaBezKart")
    @Expose
    private String uslPopolnenieSchetaBezKart;
    @SerializedName("usl_popolnenieSchetaBynIspKarts")
    @Expose
    private String uslPopolnenieSchetaBynIspKarts;
    @SerializedName("usl_popolnenieSchetaUsdIspKarts")
    @Expose
    private String uslPopolnenieSchetaUsdIspKarts;
    @SerializedName("usl_pov")
    @Expose
    private String uslPov;
    @SerializedName("usl_priemDocPokupkaObl")
    @Expose
    private String uslPriemDocPokupkaObl;
    @SerializedName("usl_priem_cennostei_na_hranenie")
    @Expose
    private String uslPriemCennosteiNaHranenie;
    @SerializedName("usl_priem_cennostej_na_hranenie")
    @Expose
    private String uslPriemCennostejNaHranenie;
    @SerializedName("usl_priem_docs_fl_depozit_operations")
    @Expose
    private String uslPriemDocsFlDepozitOperations;
    @SerializedName("usl_priem_docs_vidacha_sopr_lgot_ipotech")
    @Expose
    private String uslPriemDocsVidachaSoprLgotIpotech;
    @SerializedName("usl_priem_doc_na_kredits_magnit")
    @Expose
    private String uslPriemDocNaKreditsMagnit;
    @SerializedName("usl_priem_doc_na_kredits_overdrafts")
    @Expose
    private String uslPriemDocNaKreditsOverdrafts;
    @SerializedName("usl_priem_doc_na_lizing")
    @Expose
    private String uslPriemDocNaLizing;
    @SerializedName("usl_priem_inkasso")
    @Expose
    private String uslPriemInkasso;
    @SerializedName("usl_priem_obl_mf")
    @Expose
    private String uslPriemOblMf;
    @SerializedName("usl_priem_platejei_byn_ip")
    @Expose
    private String uslPriemPlatejeiBynIp;
    @SerializedName("usl_priem_platejei_eur_ip")
    @Expose
    private String uslPriemPlatejeiEurIp;
    @SerializedName("usl_priem_zayvleniy_obsluzhivanie_derzhatelej")
    @Expose
    private String uslPriemZayvleniyObsluzhivanieDerzhatelej;
    @SerializedName("usl_prodaga_monet")
    @Expose
    private String uslProdagaMonet;
    @SerializedName("usl_pupil_card")
    @Expose
    private String uslPupilCard;
    @SerializedName("usl_razmen_foreign_val")
    @Expose
    private String uslRazmenForeignVal;
    @SerializedName("usl_razm_prodazha_documentar_obligacii")
    @Expose
    private String uslRazmProdazhaDocumentarObligacii;
    @SerializedName("usl_rb_card")
    @Expose
    private String uslRbCard;
    @SerializedName("usl_registration_val_dogovor")
    @Expose
    private String uslRegistrationValDogovor;
    @SerializedName("usl_return_BynIspKarts")
    @Expose
    private String uslReturnBynIspKarts;
    @SerializedName("usl_return_UsdIspKarts")
    @Expose
    private String uslReturnUsdIspKarts;
    @SerializedName("usl_rko")
    @Expose
    private String uslRko;
    @SerializedName("usl_seif")
    @Expose
    private String uslSeif;
    @SerializedName("usl_strahovanie_avto")
    @Expose
    private String uslStrahovanieAvto;
    @SerializedName("usl_strahovanie_avto_pogran")
    @Expose
    private String uslStrahovanieAvtoPogran;
    @SerializedName("usl_strahovanie_detei")
    @Expose
    private String uslStrahovanieDetei;
    @SerializedName("usl_strahovanie_dohod_pod_zaschitoy")
    @Expose
    private String uslStrahovanieDohodPodZaschitoy;
    @SerializedName("usl_strahovanie_express")
    @Expose
    private String uslStrahovanieExpress;
    @SerializedName("usl_strahovanie_green_karta")
    @Expose
    private String uslStrahovanieGreenKarta;
    @SerializedName("usl_strahovanie_home")
    @Expose
    private String uslStrahovanieHome;
    @SerializedName("usl_strahovanie_kartochki")
    @Expose
    private String uslStrahovanieKartochki;
    @SerializedName("usl_strahovanie_kasko")
    @Expose
    private String uslStrahovanieKasko;
    @SerializedName("usl_strahovanie_komplex")
    @Expose
    private String uslStrahovanieKomplex;
    @SerializedName("usl_strahovanie_medicine_nerezident")
    @Expose
    private String uslStrahovanieMedicineNerezident;
    @SerializedName("usl_strahovanie_perevozki")
    @Expose
    private String uslStrahovaniePerevozki;
    @SerializedName("usl_strahovanie_s_zabotoi_o_blizkih")
    @Expose
    private String uslStrahovanieSZabotoiOBlizkih;
    @SerializedName("usl_strahovanie_timeAbroad")
    @Expose
    private String uslStrahovanieTimeAbroad;
    @SerializedName("usl_strahovanie_zashhita_ot_kleshha")
    @Expose
    private String uslStrahovanieZashhitaOtKleshha;
    @SerializedName("usl_strahovka_site")
    @Expose
    private String uslStrahovkaSite;
    @SerializedName("usl_striz")
    @Expose
    private String uslStriz;
    @SerializedName("usl_stroysber")
    @Expose
    private String uslStroysber;
    @SerializedName("usl_subsidiya_scheta")
    @Expose
    private String uslSubsidiyaScheta;
    @SerializedName("usl_swift")
    @Expose
    private String uslSwift;
    @SerializedName("usl_vklad")
    @Expose
    private String uslVklad;
    @SerializedName("usl_vozvrat_nds")
    @Expose
    private String uslVozvratNds;
    @SerializedName("usl_vydacha_nal_v_banke")
    @Expose
    private String uslVydachaNalVBanke;
    @SerializedName("usl_vydacha_vypiski")
    @Expose
    private String uslVydachaVypiski;
    @SerializedName("usl_vypllata_bel_rub")
    @Expose
    private String uslVypllataBelRub;
    @SerializedName("usl_vzk")
    @Expose
    private String uslVzk;
    @SerializedName("usl_wu")
    @Expose
    private String uslWu;
    @SerializedName("usl_plategi_all")
    @Expose
    private String uslPlategiAll;
    @SerializedName("usl_plategi_in_foreign_val")
    @Expose
    private String uslPlategiInForeignVal;
    @SerializedName("usl_plategi_za_proezd_v_polzu_banka")
    @Expose
    private String uslPlategiZaProezdVPolzuBanka;
    @SerializedName("usl_plategi_minus_mobi")
    @Expose
    private String uslPlategiMinusMobi;
    @SerializedName("usl_plategi_minus_internet")
    @Expose
    private String uslPlategiMinusInternet;
    @SerializedName("usl_plategi_minus_mobi_internet_full")
    @Expose
    private String uslPlategiMinusMobiInternetFull;
    @SerializedName("usl_plategi_nal_minus_krome_kredit")
    @Expose
    private String uslPlategiNalMinusKromeKredit;
    @SerializedName("filial_num")
    @Expose
    private String filialNum;
    @SerializedName("cbu_num")
    @Expose
    private String cbuNum;
    @SerializedName("otd_num")
    @Expose
    private String otdNum;

    @Override
    public String getAddress() {
        return streetType + street + " " + homeNumber;
    }

    @Override
    public gpsPoint getGpsPoint() {
        return new gpsPoint(Double.parseDouble(gpsX), Double.parseDouble(gpsY));
    }

    @Override
    public int getTypeId() {
        return R.string.filial;
    }
}