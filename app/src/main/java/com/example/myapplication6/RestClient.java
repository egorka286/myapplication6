package com.example.myapplication6;

import retrofit2.Retrofit;
import retrofit2.adapter.rxjava3.RxJava3CallAdapterFactory;
import retrofit2.converter.gson.GsonConverterFactory;

public class RestClient {

    private static final String baseUrl = "https://belarusbank.by/";
    private static AtmApi atmApi;
    private static InfoboxApi infoboxApi;
    private static FilialApi filialApi;
    private static final RestClient instance = new RestClient();

    private RestClient() {
        Retrofit retrofit = new Retrofit.Builder()
                .baseUrl(baseUrl)
                .addConverterFactory(GsonConverterFactory.create())
                .addCallAdapterFactory(RxJava3CallAdapterFactory.create())
                .build();
        atmApi = retrofit.create(AtmApi.class);
        infoboxApi = retrofit.create(InfoboxApi.class);
        filialApi = retrofit.create(FilialApi.class);
    }

    public static RestClient instance() {
        return instance;
    }

    public AtmApi getAtmApi() {
        return atmApi;
    }

    public InfoboxApi getInfoboxApi() {
        return infoboxApi;
    }

    public FilialApi getFilialApi() {
        return filialApi;
    }
}
