package com.example.myapplication6;

import java.util.List;

import io.reactivex.rxjava3.core.Observable;
import retrofit2.http.GET;
import retrofit2.http.Query;

public interface AtmApi {
    @GET("api/atm")
    Observable<List<Atm>> getData(@Query("city") String city);
}
