package com.example.myapplication6;

import androidx.lifecycle.LiveData;
import androidx.lifecycle.MutableLiveData;
import androidx.lifecycle.ViewModel;

import java.util.Collections;
import java.util.List;

import io.reactivex.rxjava3.android.schedulers.AndroidSchedulers;
import io.reactivex.rxjava3.core.Observable;
import io.reactivex.rxjava3.disposables.Disposable;
import io.reactivex.rxjava3.schedulers.Schedulers;

public class MainViewModel extends ViewModel {

    private MutableLiveData<List<BankObject>> mutableLiveData = null;

    public LiveData<List<BankObject>> getBankObjects() {
        if(mutableLiveData == null) {
            mutableLiveData = new MutableLiveData<>();
            loadBankObjects();
        }
        return mutableLiveData;
    }

    private void loadBankObjects() {
        String city = "Гомель";
        BankObject.gpsPoint startPoint = new BankObject.gpsPoint(52.425163, 31.015039);
        AtmApi atmApi = RestClient.instance().getAtmApi();
        InfoboxApi infoboxApi = RestClient.instance().getInfoboxApi();
        FilialApi filialApi = RestClient.instance().getFilialApi();

        Disposable disposable = Observable.concat(atmApi.getData(city), infoboxApi.getData(city), filialApi.getData(city))
                .flatMapIterable(x -> x)
                .toSortedList((a, b) -> {
                    Double distA = a.getGpsPoint().distance(startPoint);
                    Double distB = b.getGpsPoint().distance(startPoint);
                    return distA.compareTo(distB);
                })
                .toObservable()
                .flatMapIterable(x -> x)
                .distinctUntilChanged((a, b) -> a.getGpsPoint().equals(b.getGpsPoint()))
                .take(10)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .toList()
                .subscribe(a -> mutableLiveData.setValue(Collections.unmodifiableList(a)));
    }
}
